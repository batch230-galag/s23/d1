console.log("Thank you Lord!");

function divider(sectionName) {
    console.log(`----- ${sectionName} -----`);
};

divider("Objects");
// [SECTION] Objects
/*
    An object is a data type that is used to represent real world objects.
    It is a collection of related data and/or functionalities
    Information is stored in object represented in key:value pair
        // key -> property of the object
        // value -> actual data to be stored

    Two ways in creating object in javascript
    1. Object literal notation (let object = {})
    2. Object Constructor notation
        // Object Instantaition (let object = new Object();)

*/
        // objectName
    let cellphone = {
        name: "Nokia 3210",
        manufactureDate: 1999,
    }

    /*
    
    name is a property of key with value "Nokita 3210"
    manufactureDate is a property or key with value 1999
    
    */


    console.log("Result from creating objects using initializer/literal notation: ");
    console.log(cellphone);
    console.log(typeof cellphone);

    let cellphone2 = {
        name: "Iphone 13",
        manufactureDate: 2021
    }

    console.log(cellphone2);

/*
    Create an object with a name cellphone3,
    with a property 'name' and value "Xiaomi 11t Pro"
    and another key 'manufactureDate'
*/

    let cellphone3 = {
        name: "Xiaomi 11t Pro",
        manufactureDate: 2021
    }

    console.log(cellphone3);

    // Object Constructor Notation
    divider("Object Constructor Notation")
    /*
        Creating objects using a constructor function
            Creates reusable function to crate several objects that have the same data structure (blueprint)
        Syntax:
            function objectName(keyA, keyB) {
                this.keyA = keyA;
                this.keyB = keyB;
            }

        // "this" keyword refers to the properties within the object
        // it allows the assignment of new object's properties by associating them with values received from the constructor function's parameter
    */

    function Laptop(name, manufactureDate) {
        this.name = name;
        this.manufactureDate = manufactureDate;
    }

    let laptop1 = new Laptop("Lenovo", 2008);
    console.log(laptop1);

    let laptop2 = new Laptop("Macbook Air", 2008);
    console.log(laptop2);

    let laptop3 = new Laptop("Portal R2R CCMS", 1980);
    console.log(laptop3);

    // Create an empty objects
    let computer = {};
    let myCompute = new Object();

// [SECTION] Accessing Object Properties / Keys
divider("Accessing Object Properties");
    
    // Using dot notation "."
    // Syntax -> objectName.propertyName
    console.log("Result from dot notation: " + laptop2.name);

    // Using square bracket notation
    console.log("Result from square bracket notation: " + laptop2["name"]);

    // Accessing array of objects
        // Accessing object properties using the square bracket notation and array indexes can cause confusion

    let arrayObj = [laptop1, laptop2];

    // May be confused for accessing array indexes
    console.log(arrayObj[0]["name"]);

    // this tells us that array[0] is an object by using dot notation
    console.log(arrayObj[0].name);

// [SECTION] Initializing / Adding / Deleting / Reassigning Object properties
divider("Initializing / Adding / Deleting / Reassigning Object properties")

    let car = {};
    console.log("Current value of car object: ");
    console.log(car);

    // Initializing / Adding Object properties

    car.name = "Honda Civic"
    console.log("Result from adding properties using dot notation: ");
    console.log(car);

    // Initializing / Adding Object using square bracket notation (not recommended)
    car["manufactureDate"] = 2019;
    console.log("Result from adding properties using square bracket notation: ");
    console.log(car);

    // Deleting object properties
    delete car['manufactureDate'];
    console.log("Result from deleting properties");
    console.log(car);

    // Reassigning object property values
    car.name = "Toyota Vios";
    console.log("Result from reassigning property values");
    console.log(car);

// [SECTION] Object Methods
divider("Object Methods");
    // A method is function which is a property of an object

    let person = {
        name: "John",
        talk: function() {
            console.log("Hello my name is " + this.name);
        }
    }

    console.log(person);
    person.talk();

    // creating new method in an existing object
    person.walk = function(steps) {
        console.log(this.name + " walked " + steps + " steps forward")
    }

    console.log(person);
    person.walk(50);

    let friend = {
        firstName: "Joe",
        lastName: "Smith",
        address: {
            city: "Austin",
            country: "Texas"
            },
        email: ["joe@mail.com", "joesmith@mail.xyz"],
        introduce: function() {
            console.log("Hello my name is " + this.firstName + " " + this.lastName + "." + " I live in " + this.address.city + ", " + this.address.country)
        },
    }

    friend.introduce();

// [SECTION] Real World Application of Objects
/*
	Scenario:
	1. We would like to create a game that would have a several pokemon interact with each other.
	2. Every pokemon would have the same set of stats, properties and function.

	Stats:
	name
	level
	health = level * 2
	attack = level

*/

    // Create an object constructor to lessen the process in creating the pokemon
    function Pokemon(name, level) {
        // Properties
        this.name = name;
        this.level = level;
        this.health = level * 2;
        this.attack = level;
    
        // Method 
        // "target" parameter reperesent another pokemon object
        this.tackle = function(target) {
            console.log(this.name + " tackled " + target.name);
            target.health -= this.attack;
            console.log(`target Pokemon's health is now reduced to` + target.health);
            // console.log(`${target.name}'s health is now reduced to` + target.health);
        }

        this.faint = function() {
            console.log(this.name + " fainted ")
        }

    }

    let pikachu = new Pokemon("pikachu", 99);
    console.log(pikachu);

    let rattata = new Pokemon("Rattata", 5);
    console.log(rattata);

    pikachu.tackle(rattata);